import { useEffect } from 'react';
import '../components/css/style.css'

const mystyle = {
    width: "60%",
    height: "60%", 
    borderRadius: "50%"
  };
  
const Home = () => {

    useEffect(() => {
        if(window.loadOwlCarousel){
            window.loadOwlCarousel()
        }
    }, [])

    return (
        <>
            <div class="container mt-5" id="OurServices">
                <div class="row our-services">
                    <div class="col" id="our-services-margin1">
                        <br/><img src="/assets/img/img-our-services.svg" class="img-fluid" alt="orang" />
                    </div>
                    <div class="col" id="our-services-margin2">
                        <h4><b>Best Car Rental for any kind of trip in (Lokasimu)!</b></h4>
                        <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain,
                            kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
                        </p>
                        <p><img class="me-3" src="/assets/img/icon-check-fill.svg" alt="" />Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                        <p><img class="me-3" src="/assets/img/icon-check-fill.svg" alt="" />Sewa Mobil Lepas Kunci di Bali 20 Jam</p>
                        <p><img class="me-3" src="/assets/img/icon-check-fill.svg" alt="" />Sewa Mobil Jangka Panjang Bulanan</p>
                        <p><img class="me-3" src="/assets/img/icon-check-fill.svg" alt="" />Gratis Antar - Jemput Mobil di Bandara</p>
                        <p><img class="me-3" src="/assets/img/icon-check-fill.svg" alt="" />Layanan Airport Transfer / Drop In Out</p>
                    </div>
                </div>
            </div>

            <div class="container mt-5" id="WhyUs">
                <h4 class="hw"><b>Why Us?</b></h4>
                <p class="hw">Mengapa harus pilih Binar Car Rental?</p>
                <div class="row">
                    <div class="col-lg">
                        <div class="card whyus">
                            <div class="card-body">
                                <i class="btn-warning btn-sm bi-hand-thumbs-up why-us-icon" style={{color:'white'}}></i>
                                <h5 class="card-title mt-2"><b>Mobil Lengkap</b></h5>
                                <p class="card-text">Tersedia banyak pilihan mobil dengan kondisi masih baru yang bersih dan terawat</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg">
                        <div class="card whyus">
                            <div class="card-body">
                                <i class="btn-danger btn-sm bi-tag  why-us-icon"></i>
                                <h5 class="card-title mt-2"><b>Harga Murah</b></h5>
                                <p class="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg">
                        <div class="card whyus">
                            <div class="card-body">
                                <i class="btn-primary btn-sm bi-clock  why-us-icon"></i>
                                <h5 class="card-title mt-2"><b>Layanan 24 Jam</b></h5>
                                <p class="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir
                                    minggu</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg">
                        <div class="card whyus">
                            <div class="card-body">
                                <i class="btn-success btn-sm bi-award why-us-icon"></i>
                                <h5 class="card-title mt-2"><b>Sopir Profesional</b></h5>
                                <p class="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid hp">
                <h4 class="text-center" id="Testimonial"><b>Testimonial</b></h4>
                <p class="text-center">Berbagai review positif dari para pelanggan kami</p>
            </div>

            <div class="row" style={{overflow: 'hidden', marginRight: 0}}>
                <div class="large-12 columns">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="card testimonial-card h-100 me-2">
                                <div class="card-body ">
                                    <br/>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <img src="/assets/img/img-face3.svg" class="img-fluid rounded-circle mx-auto mt-4 d-block"
                                                style={mystyle} alt="" />
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="star">
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                            </div>
                                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                                                amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod”</p>
                                            <b>John Dee 31, Bromo</b>
                                        </div>
                                    </div><br/>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card testimonial-card h-100 me-2">
                                <div class="card-body ">
                                    <br/>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <img src="/assets/img/img-face2.svg" class="img-fluid rounded-circle mx-auto mt-4 d-block"
                                                style={mystyle} alt="" />
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="star">
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                            </div>
                                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                                                amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod”</p>
                                            <b>John Dee 31, Bromo</b>
                                        </div>
                                    </div><br/>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card testimonial-card h-100 me-2">
                                <div class="card-body ">
                                    <br/>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <img src="/assets/img/img-face3.svg" class="img-fluid rounded-circle mx-auto mt-4 d-block"
                                                style={mystyle} alt="" />
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="star">
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                            </div>
                                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                                                amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod”</p>
                                            <b>John Dee 31, Bromo</b>
                                        </div>
                                    </div><br/>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="card testimonial-card h-100 me-2">
                                <div class="card-body ">
                                    <br/>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <img src="/assets/img/img-face3.svg" class="img-fluid rounded-circle mx-auto mt-4 d-block"
                                                style={mystyle} alt="" />
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="star">
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                            </div>
                                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                                                amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod”</p>
                                            <b>John Dee 31, Bromo</b>
                                        </div>
                                    </div><br/>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card testimonial-card h-100 me-2">
                                <div class="card-body ">
                                    <br/>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <img src="/assets/img/img-face2.svg" class="img-fluid rounded-circle mx-auto mt-4 d-block"
                                                style={mystyle} alt="" />
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="star">
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                            </div>
                                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                                                amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod”</p>
                                            <b>John Dee 31, Bromo</b>
                                        </div>
                                    </div><br/>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card testimonial-card h-100 me-2">
                                <div class="card-body ">
                                    <br/>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <img src="/assets/img/img-face3.svg" class="img-fluid rounded-circle mx-auto mt-4 d-block"
                                                style={mystyle} alt="" />
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="star">
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                                <i class="bi-star-fill text-warning"></i>
                                            </div>
                                            <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                                                amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod”</p>
                                            <b>John Dee 31, Bromo</b>
                                        </div>
                                    </div>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container hwp"><br/>
                <br/>
                <div class="card" id="cta-banner">
                    <div class="card-body mx-auto align-items-center text-center">
                        <h1 class="mt-5"><b>Sewa Mobil di (Lokasimu) Sekarang</b></h1><br/>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br/>tempor incididunt ut labore et
                            dolore magna aliqua.</p><br/><br/>
                        <button class="btn btn-success mb-5">Mulai Sewa Mobil</button>
                    </div>
                </div>
            </div>

            <div class="container mt-5" id="FAQ">
                <br/>
                <div class="row" id="faq">
                    <div class="col">
                        <h4 class="hw"><b>Frequently Asked Question</b></h4>
                        <p class="hw">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    </div>
                    <div class="col">
                        <div class="accordion" id="accordionFlushExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="false"
                                        aria-controls="panelsStayOpen-collapseOne">
                                        Apa saja syarat yang dibutuhkan?
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show"
                                    aria-labelledby="panelsStayOpen-headingOne">
                                    <div class="accordion-body">
                                        This is the first item's accordion body. It is shown by default, until the collapse plugin adds the
                                        appropriate classes that we use to style each element. These classes control the overall appearance,
                                        as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or
                                        overriding our default variables. It's also worth noting that just about any HTML can go within the
                                        .accordion-body, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item mt-3" style={{borderTop: '1 solid rgba(0,0,0.125'}}>
                                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false"
                                        aria-controls="panelsStayOpen-collapseTwo">
                                        Berapa hari minimal sewa mobil lepas kunci?
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse"
                                    aria-labelledby="panelsStayOpen-headingTwo">
                                    <div class="accordion-body">
                                        This is the second item's accordion body. It is hidden by default, until the collapse plugin adds the
                                        appropriate classes that we use to style each element. These classes control the overall appearance,
                                        as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or
                                        overriding our default variables. It's also worth noting that just about any HTML can go within the
                                        .accordion-body, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item mt-3" style={{borderTop: '1 solid rgba(0,0,0.125'}}>
                                <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false"
                                        aria-controls="panelsStayOpen-collapseThree">
                                        Berapa hari sebelumnya sabaiknya booking sewa mobil?
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse"
                                    aria-labelledby="panelsStayOpen-headingThree">
                                    <div class="accordion-body">
                                        This is the third item's accordion body. It is hidden by default, until the collapse plugin adds the
                                        appropriate classes that we use to style each element. These classes control the overall appearance,
                                        as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or
                                        overriding our default variables. It's also worth noting that just about any HTML can go within the
                                        .accordion-body, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item mt-3" style={{borderTop: '1 solid rgba(0,0,0.125'}}>
                                <h2 class="accordion-header" id="panelsStayOpen-headingFour">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#panelsStayOpen-collapseFour" aria-expanded="false"
                                        aria-controls="panelsStayOpen-collapseFour">
                                        Apakah Ada biaya antar-jemput?
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseFour" class="accordion-collapse collapse"
                                    aria-labelledby="panelsStayOpen-headingFour">
                                    <div class="accordion-body">
                                        This is the fourth item's accordion body. It is hidden by default, until the collapse plugin adds the
                                        appropriate classes that we use to style each element. These classes control the overall appearance,
                                        as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or
                                        overriding our default variables. It's also worth noting that just about any HTML can go within the
                                        .accordion-body, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item mt-3" style={{borderTop: '1 solid rgba(0,0,0.125'}}>
                                <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="false"
                                        aria-controls="panelsStayOpen-collapseFive">
                                        Bagaimana jika terjadi kecelakaan
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse"
                                    aria-labelledby="panelsStayOpen-headingFive">
                                    <div class="accordion-body">
                                        This is the fifth item's accordion body. It is hidden by default, until the collapse plugin adds the
                                        appropriate classes that we use to style each element. These classes control the overall appearance,
                                        as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or
                                        overriding our default variables. It's also worth noting that just about any HTML can go within the
                                        .accordion-body, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>

    )
}

export default Home