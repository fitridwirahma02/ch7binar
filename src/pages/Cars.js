import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAsyncData, setFilter } from "../reducers/api-store";
import '../components/css/cars.css'

import capacityLogo from "../fi_users.png";
import gearLogo from "../fi_settings.png";
import calendarLogo from "../fi_calendar.png";


function rupiah(price) {
    let convertPrice = price.toString();
    let convertString = convertPrice.split("");
    let array = [];
    let temp = 3;

    for (let i = convertString.length - 1; i >= 0; i--) {
        temp -= 1;
        array.unshift(convertString[i]);
        if (temp === 0 && i !== 0) {
            array.unshift(".");
            temp = 3;
        }
    }
    return array.join("");
}

const Cars = () => {
    const dispatch = useDispatch();
    const listCarsJson = useSelector((state) => state.api.cars);

    const filter = useSelector((state) => state.api.filter);

    const listOfFilteredCars = [];

    const [driver, setDriver] = useState();
    const [tanggal, setTanggal] = useState();
    const [jemput, setJemput] = useState();
    const [penumpang, setPenumpang] = useState();

    const [data, setData] = useState({});

    useEffect(() => {
        dispatch(setFilter(data));
    }, [dispatch, data]);

    const handleSubmit = (e) => {
        e.preventDefault();
        setData({
            driver: driver,
            tanggal: tanggal,
            jemput: jemput,
            penumpang: parseInt(penumpang),
        });
    };


    function filteringCars() {
        let driverFilter = "";
        if (filter.driver === "1") {
            driverFilter = true;
        } else {
            driverFilter = false;
        }
        for (let i in listCarsJson) {
            let car = listCarsJson[i];
            let dateConversion = new Date(car.availableAt);
            if (car.available === true &&
                dateConversion <= new Date(filter.tanggal) &&
                car.capacity >= filter.penumpang &&
                car.driver === driverFilter
            ) {
                listOfFilteredCars.push(listCarsJson[i]);
            }
        }
    }
    filteringCars();
    useEffect(() => {
        dispatch(getAsyncData());
    }, [dispatch]);

    return (
        <>
            <div className="container mt-4">
                <div id="filter-box">
                    <div className="card mb-4">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-lg-10 col-md-12">
                                    <form className="needs-validation" noValidate>
                                        <div className="row">
                                            <div className="col-lg-3 col-md-6">
                                                <div className="form-group" id="form-group1"><label>Tipe Driver</label>
                                                    <select id="select-driver" className="form-control" onChange={(e) => setDriver(e.target.value)}>
                                                        <option>Pilih Tipe Driver</option>
                                                        <option value="1">Dengan Sopir</option>
                                                        <option value="2">Tanpa Sopir</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-md-6">
                                                <div className="form-group" id="form-group2">
                                                    <label>Tanggal</label>
                                                    <input id="tanggal" type="date" className="form-control" onChange={(e) => setTanggal(e.target.value)} />
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-md-6">
                                                <div className="form-group" id="form-group3">
                                                    <label>Waktu Jemput/Ambil</label>
                                                    <input id="waktu" type="time" className="form-control" onChange={(e) => setJemput(e.target.value)} />
                                                </div>
                                            </div>
                                            <div className="col-lg-3 col-md-6">
                                                <div className="form-group" id="form-group4">
                                                    <label>Jumlah Penumpang</label>
                                                    <div className="input-group">
                                                        <input id="penumpang" type="number" className="form-control border-end-0"
                                                            placeholder="Jumlah Penumpang" onChange={(e) => setPenumpang(e.target.value)} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-lg-2 col-md-12">
                                    <div className="form-group" id="form-group5"><label>&nbsp;</label>
                                        <button id="button-cari" type="button" className="btn btn-success col-sm-12" onClick={handleSubmit}>Cari Mobil</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row" id="cars-list">
                    {listOfFilteredCars.map((car) => {
                        return (
                            <div className="col-lg-4 d-flex justify-content-center" style={{ marginTop: "40px" }}
                                key={car.id}>
                                <div className="listcar-card card" style={{ width: "25rem" }}>
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <img src={car.image} style={{ objectfit: "contain", maxHeight: "250px" }}
                                                className="card-img-top" alt={car.manufacture} />
                                        </div>
                                    </div>

                                    <div className="card-body" style={{ fontfamily: "Helvetica" }}>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                {car.manufacture} {car.model}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <b>Rp {rupiah(car.rentPerDay)} / hari</b>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <p>{car.description}</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12"></div>
                                        </div>
                                        <img src={capacityLogo} width="16" height="16" alt={capacityLogo} />
                                        {car.capacity} orang
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <img src={gearLogo} width="16" height="16" alt={gearLogo} />
                                                {" "}
                                                {car.transmission}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <img src={calendarLogo} width="16" height="16" alt={calendarLogo} />
                                                Tahun {car.year}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-footer">
                                        <button className="btn btn-success col-lg-12">
                                            Pilih Mobil
                                        </button>
                                    </div>
                                </div>
                            </div>
                        );
                    })}

                </div>
                
            </div>
        </>
    )
}

export default Cars