const mystyle = {
    width: "100%",
    fontSize: "14px",
    lineHeight: "25px"
  };

const Footer = () => {
    return (
        <footer class="footer">
            <div class="container pt-5" style={mystyle}>
                <div class="row text-left">
                    <div class="col-lg-3" style={{listStyleType: 'none'}}>
                        <li class="mb-3">Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</li>
                        <li class="mb-3">binarcarrental@gmail.com</li>
                        <li class="mb-3">081-233-334-808</li>
                    </div>

                    <div class="col-lg-3" style={{fontWeight: "400 bold", listStyleType: 'none'}}>
                        <li class="mb-2">Our Services</li>
                        <li class="mb-2">Why Us</li>
                        <li class="mb-2">Testimonial</li>
                        <li class="mb-2">FAQ</li>
                    </div>

                    <div class="col-lg-3">
                        <p>Connect With Us</p>
                        <i class="btn-primary btn-sm bi-facebook me-2 icon-footer"></i>
                        <i class="btn-primary btn-sm bi-instagram me-2 icon-footer"></i>
                        <i class="btn-primary btn-sm bi-twitter me-2 icon-footer"></i>
                        <i class="btn-primary btn-sm bi-envelope me-2 icon-footer"></i>
                        <i class="btn-primary btn-sm bi-twitch me-2 icon-footer"></i>
                    </div>

                    <div class="col-lg-3">
                        <p>Copyright Binar 2022</p>
                        <a href="/" class="btn btn-primary" id="logo2">&nbsp;</a>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer