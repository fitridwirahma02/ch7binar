import './css/style.css';

const Header = () => {
    return (
        <div className="container-fluid mt-5 bg-light">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <img src="/assets/img/img-car.svg" className="img-fluid rounded float-end" id="mobil" alt="mobil" />
                        <h1><br /><b>Sewa dan Rental Mobil Terbaik di kawasan (Lokasimu)</b></h1>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau.
                            Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                        <a href="/cars" className="btn btn-success">Mulai Sewa Mobil</a>
                        <img src="/assets/img/img-car.svg" id="mobil1" alt="mobil" />
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Header
