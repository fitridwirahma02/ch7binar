import './css/style.css';
const mystyle = {
    width: "50%"
  };

const Navbar = () => {
    return (

        <nav className="navbar navbar-expand-lg navbar-light fixed-top bg-light">
            <div className="container">
                <a className="navbar-brand me-auto" id="logo" href="/">&nbsp;</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight"
                    aria-controls="offcanvasRight" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel"
                    style={mystyle}>
                    <div className="offcanvas-header">
                        <h5 id="offcanvasRightLabel"><b>BCR</b></h5>
                        <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div className="offcanvas-body">
                        <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li className="nav-item me-2">
                                <a className="nav-link active" aria-current="page" href="#OurServices">Our Services</a>
                            </li>
                            <li className="nav-item me-2">
                                <a className="nav-link active" href="#WhyUs">Why Us</a>
                            </li>
                            <li className="nav-item me-2">
                                <a className="nav-link active" href="#Testimonial">Testimonial</a>
                            </li>
                            <li className="nav-item me-3">
                                <a className="nav-link active" href="#FAQ">FAQ</a>
                            </li>
                            <li className="nav-item">
                                <a className="btn btn-success" href="#Register">Register</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>


    )
}

export default Navbar
